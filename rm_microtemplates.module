<?php
/**
 * @file
 * Allow users to create themes for defined regions
 */
 
 
/**
 * Implementation of hook_menu
 */
function rm_microtemplates_menu() {
	$items['admin/config/rm_microtemplates'] = array(
		'title' => 'RM_Microtemplates',
		'description' => 'Allows users to create microtemplates for specified regions.',
		'position' => 'right',
		'weight' => -5,
		'page callback' => 'system_admin_menu_block_page',
		'access arguments' => array('administer site configuration'),
		'file' => 'system.admin.inc',
		'file path' => drupal_get_path('module', 'system'),
	);
	
	$items['admin/config/rm_microtemplates/settings'] = array(
		'title' => 'RM_Microtemplates settings',
		'description' => 'Configure settings for RM_Microtemplates',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('rm_microtemplates_admin_settings'),
		'access arguments' => array('administer site configuration'),
		'type' => MENU_NORMAL_ITEM,
		'file' => 'rm_microtemplates.admin.inc',
	);
	
	return $items;
}


/**
 * Implements hook_menu_alter().
 */
function rm_microtemplates_menu_alter(&$callbacks) {
  // If the user does not have 'administer nodes' permission,
  // disable the job_post menu item by setting its access callback to FALSE.
  if (!user_access('administer nodes')) {
    $callbacks['node/add/rm_microtemplates_backgrounds']['access callback'] = FALSE;
    // Must unset access arguments or Drupal will use user_access()
    // as a default access callback.
    unset($callbacks['node/add/rm_microtemplates_bakcgrounds']['access arguments']);
  }
}




/**
 * Implements hook_node_info()
 */
function rm_microtemplates_node_info() {
  return array(
    'rm_microtemplates_backgrounds' => array(
      'name' => t('RM Microtemplates » Backgrounds'),
      'base' => 'rm_microtemplates_backgrounds',
      'description' => t('Use this content type to crete a new background preset.'),
      'has_title' => FALSE,
      'help' => t('Upload the media you want to show as background to your posts'),
    ),
  );
}


/**
 * Impelements hook_permission()
 * Define permissions of the module
 */
function rm_microtemplates_permission() {
	return array(
		'view microtemplated content' => array(
			'title' => t('View microtemplated content'),
		),
		'add_edit own microtemplates' => array(
			'title' => t('Add/edit own microtemplates')	
		),
		'add_edit default microtemplates' => array(
			'title' => t('Add/edit default microtemplates'),
		),
		'add_edit all microtemplates' => array(
			'title' => t('Add/edit all microtemplates'),
		),
		'delete own microtemplates' => array(
			'title' => t('Delete own microtemplates'),
		),
		'delete default microtemplates' => array(
			'title' => t('Delete default microtemplates')
		),
		'delete all microtemplates' => array(
			'title' => t('Delete all microtemplates')
		),
		'allow image backgrounds' => array(
			'title' => t('Allow image backgrounds')
		),
		'allow video backgrounds' => array(
			'title' => t('Allow video backgrounds')
		),
		'allow canvas backgrounds' => array(
			'title' => t('Allow canvas backgrounds')
		),
		'allow swf backgrounds' => array(
			'title' => t('Allow swf backgrounds')
		),
		'allow audio backgrounds' => array(
			'title' => t('Allow audio backgrounds')
		),
		'allow template sharing' => array(
			'title' => t('Allow template sharing')
		),
	);
}


/**
 * Implements hook_node_access().
 */
function rm_microtemplates_node_access($node, $op, $account) {
  
  // get the node type
  $type = is_string($node) ? $node : $node->type;
  
  // if other node type just ignore the rest
  if ($type !== 'rm_microtemplates_backgrounds') {
    return NODE_ACCESS_IGNORE;
  }
  
  if (is_string($node)) {
    $is_author = FALSE;
  } else {
    $is_author = $account->uid == $node->uid;
  }
  
  
  switch ($op) {
    case 'create':
      // Allow if user's role has 'add_edit own microtemplates' permission.
      if (user_access('add_edit own microtemplates', $account)) {
         return NODE_ACCESS_ALLOW;
      }

    case 'update':
      // Allow if user's role has 'add_edit own microtemplates' permission and user is
      // the author; or if the user's role has 'add_edit all microtemplates' permission.
      if (user_access('add_edit own microtemplates', $account) && $is_author ||
          user_access('add_edit all microtemplates', $account)) {
            return NODE_ACCESS_ALLOW;
          }

    case 'delete':
      // Allow if user's role has 'delete own microtemplates' permission and user is
      // the author; or if the user's role has 'delete all microtemplates' permission.
      if (user_access('delete own microtmeplates', $account) && $is_author ||
          user_access('delete all microtemplates', $account)) {
            return NODE_ACCESS_ALLOW;
          }
    }
}