<?php

/**
 * @file
 * Administration page callbacks for the rm_microtemplates module
 */
 
 
/**
 * Form builder.
 *
 * @ingroup forms
 * @see system_setting_form()
 */
function rm_microtemplates_admin_settings() {
  
  $form['contenttypes_fieldset'] = array(
    '#title' => t("Content types"),
    // The prefix/suffix provide the div that we're replacing, named by
    // #ajax['wrapper'] above.
    '#prefix' => '<div id="contenttypes-div">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
    '#description' => t('Microtemplating will be available for those content types'),  
  );

	$types = node_type_get_types();
 	foreach ($types as $node_type) {
 		$options[$node_type->type] = $node_type->name;
 	}
 	
 	$form['contenttypes_fieldset']['rm_microtemplates_node_types'] = array(
 		'#type' => 'checkboxes',
 		'#title' => t('Users may create microtemplates for these content types'),
 		'#options' => $options,
 		'#default_value' => variable_get('rm_microtemplates_node_types', array('page')),
 		'#description' => t('Options to select microtemplates will be available on these content types'),
 	);
  
  $form['embedding_fieldset'] = array(
    '#title' => t("Embedding details"),
    // The prefix/suffix provide the div that we're replacing, named by
    // #ajax['wrapper'] above.
    '#prefix' => '<div id="checkboxes-div">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
    '#description' => t('You can choose the embed-method here'),  
  );
 	
	
	$options = array(
		0 => t('Theme region'),
		1 => t('Custom CSS selector'),
	);
 	$form['embedding_fieldset']['rm_microtemplates_choose_container_type'] = array(
 		'#type' => 'radios',
 		'#title' => t('Choose the method to include the RM_Microtemplates container'),
 		'#options' => $options,
 		'#default_value' => variable_get('rm_microtemplates_choose_container_type', 0),
 		'#description' => t('Depending on your selection you will find settings to configure your  microtemplates'),
 	);
  
  $form['embedding_fieldset']['rm_microtemplates_css_sel'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS root selector'),
    '#description' => t('Enter the CSS selector of the microtemplateable root element'),
    '#default_value' => variable_get('rm_microtemplates_css_sel', 'div.rm_microtemplates_container'),
    '#states' => array(
      'visible' => array(
        'input[name="rm_microtemplates_choose_container_type"]' => array('value' => '1'),
      )
    )
  );

 	$form['#submit'][] = 'rm_microtemplates_admin_settings_submit';
 	return system_settings_form($form);
}

/**
 * Process rm_microtemplates settings submission
 */
function rm_microtemplates_admin_settings_submit($form, $form_state) {
	foreach ( $form_state['values']['rm_microtemplates_node_types'] as $key => $value ) {

		// If check box for a content type is unchecked:
		if (!$value) {
		
			watchdog( 'RM_Microtemplates', 
								'Unchecked availability for content type %key', 
								array('%key' => $key));
			
		} else {
		
			// If check box for a content type is checked:
			
			watchdog( 'RM_Microtemplates', 
								'Checked availability for content type %key', 
								array('%key' => $key));
			
		}
	}
}